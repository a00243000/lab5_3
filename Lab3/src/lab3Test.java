import java.io.FileNotFoundException;

import junit.framework.TestCase;


public class lab3Test extends TestCase {
	
	public void TestcountWords() throws FileNotFoundException
	{
		//Test No: 1
		//Test Obj: Count Words
		//Inputs :Lab3.txt
		//Expected Ouputs : 244
		lab3 l = new lab3();
		assertEquals(244, l.countWords());
	}
	
	public void TestcountVowels() throws FileNotFoundException
	{
		//Test No: 2
		//Test Obj: Count Vowels
		//Inputs :Lab3.txt
		//Expected Ouputs : 277
		lab3 l = new lab3();
		assertEquals(277, l.countVowels());
	}
	public void TestavgVowels() throws FileNotFoundException
	{
		//Test No: 3
		//Test Obj: Avg Vowels
		//Inputs :277,244
		//Expected Ouputs : 1.0
		lab3 l = new lab3();
		assertEquals(1.0, l.avgVowels(277,244));
	}
}
