import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class lab3 {	
	public static int countWords() throws FileNotFoundException{
		File f= new File("E:\\Lab3.txt");
		@SuppressWarnings("resource")
		Scanner sc= new Scanner(f);
		String s=sc.nextLine();
		int wordCount=0;
		
		boolean word=false;
		int endOfLine=s.length()-1;
		
		for(int i = 0;i<s.length();i++){
			
			if (Character.isLetter(s.charAt(i)) && i !=endOfLine)
			{
				word= true;
			}
			else if( !Character.isLetter(s.charAt(i)) && word)
			{	
				wordCount++;
				word=false;
			}
			else if (Character.isLetter(s.charAt(i)) && i== endOfLine)
			{
				wordCount++;
			}
		}
		return wordCount;
	}
	
	public static int countVowels() throws FileNotFoundException
	{
		File f= new File("E:\\Lab3.txt");
		@SuppressWarnings("resource")
		Scanner sc= new Scanner(f);
		String s=sc.nextLine();
		int countVowels =0;
		for(int i = 0;i<s.length();i++)
		{
			char ch=s.charAt(i);
				if(ch == 'a'|| ch =='e'||ch=='i'||ch=='0'||ch=='u')
				{
					countVowels++;
				}
		}
		return countVowels;
	}
	
	public static double avgVowels(int countVowels,int countWords)
	{
		double avgVowels= countVowels / countWords;
		return avgVowels;
	}
	
	public static void reverseWord() throws FileNotFoundException
	{
		File f= new File("E:\\Lab3.txt");
		@SuppressWarnings("resource")
		Scanner sc= new Scanner(f);
		String s=sc.nextLine();
		char[]c=s.toCharArray();
		for(int i=0;i<c.length;i++)
		{
			swap(c,i,(c.length-(i+1)));
		}
		for (int i=0;i<c.length;i++)
		{
			System.out.print(c[i]);
		}
	}
	public static void swap(char[]c,int i,int j)
	{
		char temp=c[i];
		c[i]=c[j];
		c[j]=temp;
	}
	public static void main(String[] args) throws FileNotFoundException{
		File f= new File("E:\\Lab3.txt");
		Scanner sc= new Scanner(f);
		String s=sc.nextLine();
		System.out.println(s);
		System.out.println("Word Count: "+countWords());
		System.out.println("Vowel Count: "+countVowels());
		System.out.println("Average Vowel Count: "+avgVowels(countVowels(),countWords()));
		reverseWord();
	}
}
